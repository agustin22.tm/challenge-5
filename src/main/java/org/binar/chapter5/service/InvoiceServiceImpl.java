package org.binar.chapter5.service;

import org.binar.chapter5.model.ChairNumber;
import org.binar.chapter5.repository.FilmsRepository;
import org.binar.chapter5.repository.SchedulesRepository;
import org.binar.chapter5.repository.SeatsRepository;
import org.binar.chapter5.repository.UsersMovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class InvoiceServiceImpl implements  InvoiceService {

    @Autowired
    FilmsRepository filmRepo;

    @Autowired
    UsersMovieRepository userRepo;

    @Autowired
    SchedulesRepository schedRepo;

    @Autowired
    SeatsRepository seatRepo;


    @Override
    public List<Object> generateInvoice(String filmName, String username, Integer number, String studio) {
        Object dataFilm = filmRepo.getFilmName(filmName).getFilmName();
        Object dataUser = userRepo.getUsername(username).getUsername();
        ChairNumber chairNum = seatRepo.getChairNumber(number, studio).getChairNumber();
        Object startTime = schedRepo.getFilmSchedule(filmName).getStartTime();
        Object showDate = schedRepo.getFilmSchedule(filmName).getShowDate();
        Object endTime = schedRepo.getFilmSchedule(filmName).getEndTime();

        String numbers = chairNum.getNumber().toString();
        String studios = chairNum.getStudio().toString();
        StringBuilder chairNumber = new StringBuilder().append(numbers).append(studios);

        List<Object> invoiceList = new ArrayList<>();
        invoiceList.add(dataUser);
        invoiceList.add(dataFilm);
        invoiceList.add(showDate);
        invoiceList.add(startTime);
        invoiceList.add(endTime);
        invoiceList.add(chairNumber);

        return invoiceList;
    }


}
