package org.binar.chapter5.controller;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.binar.chapter5.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@RestController
@RequestMapping("/invoice")
public class InvoiceController {

    @Autowired
    InvoiceService invoiceService;


    @PostMapping("/ticket_movie")
    public void generateInvoice(HttpServletResponse response,
                                @RequestParam("username") String username,
                                @RequestParam("film_name") String filmName,
                                @RequestParam("number") Integer number,
                                @RequestParam("studio") String studio){

        List<Map<String,Object>> dataInvoice = new ArrayList<>();
        Map<String, Object> invoice = new HashMap<>();
        try{
            JasperReport sourceFileName = JasperCompileManager.compileReport(
                    ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "invoice.jrxml").getAbsolutePath());




            List<Object> data = invoiceService.generateInvoice(filmName, username, number, studio);

            invoice.put("username", data.get(0));
            invoice.put("filmName", data.get(1));
            invoice.put("showDate", data.get(2).toString());
            invoice.put("startTime", data.get(3).toString());
            invoice.put("endTime", data.get(4).toString());
            invoice.put("chairNumber", data.get(5).toString());
            dataInvoice.add(invoice);


            JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(dataInvoice);
            Map<String, Object> parameters = new HashMap<>();

            JasperPrint jasperPrint = JasperFillManager.fillReport(sourceFileName, parameters, beanCollectionDataSource);

            response.setContentType("application/pdf");
            response.addHeader("Content-Disposition", "inline; filename=invoice.pdf;");

            JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());


        } catch (Exception e){
            invoice.put("message", "invoice gagal digenerate!, dikarenakan" + e.getMessage());
             new ResponseEntity<>(invoice, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
