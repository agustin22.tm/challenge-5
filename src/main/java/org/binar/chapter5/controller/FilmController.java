package org.binar.chapter5.controller;

import org.binar.chapter5.model.Films;
import org.binar.chapter5.model.Schedules;
import org.binar.chapter5.service.FilmServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/film")
public class FilmController {

    @Autowired
    FilmServiceImpl filmService;

    //Mengupdate nama film
    @PutMapping("/update")
    public ResponseEntity updateFilm(@RequestParam("filmName") String filmName,
                                     @RequestParam("showing") Boolean showing,
                                     @RequestParam("filmCode") Integer filmCode) {

        Map<String, Object> resp = new HashMap<>();
        try {
            resp.put("message", "update success!");
            resp.put("nama film", filmName);
            resp.put("sedang tayang", showing);
            resp.put("kode film", filmCode);
            filmService.updateFilm(filmName, showing, filmCode);
            return new ResponseEntity(HttpStatus.CREATED);
        } catch (Exception e) {
            resp.put("message", "update gagal!, dikarenakan " + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Menambahkan film baru
    @PostMapping("/new_film")
    public ResponseEntity insertFilm(
            @RequestParam("filmName") String filmName,
            @RequestParam("showing") Boolean showing,
            @RequestParam("filmCode") Integer filmCode) {
        Map<String, Object> resp = new HashMap<>();

        try {

            resp.put("message", "insert success!");
            resp.put("nama film", filmName);
            resp.put("kode film", filmCode);
            resp.put("sedang tayang", showing);
            filmService.addNewFilm(filmName, showing, filmCode);
            return new ResponseEntity(resp, HttpStatus.CREATED);
        } catch (Exception e) {
            resp.put("message", "insert gagal!, dikarenakan  " + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Menghapus film
    @DeleteMapping("/delete")
    public ResponseEntity deleteFilm(@RequestParam("filmName") String filmName) {

        Map<String, Object> resp = new HashMap<>();
        try {
            resp.put("message", "delete success!");
            resp.put("nama film", filmName);
            filmService.deleteFilm(filmName);
            return new ResponseEntity(resp, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            resp.put("message", "delete gagal!, dikarenakan " + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    //Menampilkan list film yang sedang tayang
    @GetMapping("/search_film_by_showing")
    public ResponseEntity searchShowingFilm(@RequestParam("showing") Boolean showing) {
        Map<String, Object> resp = new HashMap<>();

        try {
            List<Films> filmsList = filmService.showingFilm(showing);

            resp.put("Film yang sedang rilis", filmsList);
            return new ResponseEntity(resp, HttpStatus.OK);
        } catch (Exception e) {
            resp.put("message", "search gagal!, dikarenakan " + e.getMessage());
            return new ResponseEntity(resp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Menampilkan list jadwal dari film tertentu
    @PostMapping("/search_schedule_by_film")
    public ResponseEntity searchFilmSchedule(
            @RequestParam("filmCode") Integer filmCode) {
        Map<String, Object> resp = new HashMap<>();
        try {
            List<Schedules> schedulesList = filmService.showingScheduleFilm(filmCode);
            resp.put("Jadwal film", schedulesList);
            return new ResponseEntity(resp, HttpStatus.CREATED);
        } catch (Exception e) {
            resp.put("message", "search gagal!, dikarenakan " + e.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
