package org.binar.chapter5.service;

import org.binar.chapter5.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


public interface InvoiceService {

     List<Object> generateInvoice(String filmName, String username, Integer number, String studio);



}
